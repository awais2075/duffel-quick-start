package com.duffel.quick.start.enums;

public enum CabinClass {
    first, business, premium_economy, economy;
}
