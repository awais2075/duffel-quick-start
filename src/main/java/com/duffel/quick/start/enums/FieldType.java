package com.duffel.quick.start.enums;

public enum FieldType {
    adult, child, infant_without_seat
}
