package com.duffel.quick.start.config;

import com.duffel.quick.start.exception.GenericException.*;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

@Component
public class FeignErrorConfig implements ErrorDecoder {

    @SneakyThrows
    @Override
    public Exception decode(String methodKey, Response response) {
        throw new DuffelServiceException(IOUtils.toString(response.body().asReader()));
    }
    
}