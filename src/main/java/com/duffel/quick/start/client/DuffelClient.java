package com.duffel.quick.start.client;

import com.duffel.quick.start.config.FeignErrorConfig;
import com.duffel.quick.start.dto.Data;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(url = "https://api.duffel.com/air", name = "duffel-quick-guide", configuration = FeignErrorConfig.class)
public interface DuffelClient {

    @PostMapping("/offer_requests")
    JsonNode searchFlights(@RequestHeader Map<String, String> headers, @RequestBody Data data, @RequestParam(value = "return_offers", required = false) Boolean returnOffers);
}
