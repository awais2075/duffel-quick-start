package com.duffel.quick.start;

import com.duffel.quick.start.config.FeignErrorConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients
public class DuffelQuickStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(DuffelQuickStartApplication.class, args);
	}

	@Bean
	public FeignErrorConfig errorDecoder() {
		return new FeignErrorConfig();
	}
}
