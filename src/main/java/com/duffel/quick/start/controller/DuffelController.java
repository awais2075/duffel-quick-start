package com.duffel.quick.start.controller;

import com.duffel.quick.start.client.DuffelClient;
import com.duffel.quick.start.dto.Data;
import com.duffel.quick.start.dto.request.FlightRequest;
import com.duffel.quick.start.service.DuffelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Component
@RestController
@RequestMapping("/duffel")
public class DuffelController {

    @Autowired
    private DuffelService duffelService;

    @PostMapping("/searchFlights")
    @ResponseStatus(HttpStatus.OK)
    public JsonNode searchFlights(@Valid @RequestBody FlightRequest flightRequest) throws JsonProcessingException {
        return duffelService.searchFlights(flightRequest);
    }
}
