package com.duffel.quick.start.exception;

import com.duffel.quick.start.dto.response.ApiError;
import com.duffel.quick.start.dto.response.DuffelApiError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@ResponseBody
public class GenericException {

    @ExceptionHandler(DuffelServiceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleException(DuffelServiceException ex) throws JsonProcessingException {
        var error = new ObjectMapper().readValue(ex.getMessage(), DuffelApiError.class);
        return buildApiErrorResponse(error.getMeta().getStatus().toString(), error.getErrors().get(0).getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleException(HttpMessageNotReadableException ex) throws JsonProcessingException {
        return buildApiErrorResponse("400", ex.getMessage());
    }

    @ExceptionHandler(InvalidDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleException(InvalidDateException ex) throws JsonProcessingException {
        return buildApiErrorResponse("400", ex.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleException(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    private ApiError buildApiErrorResponse(String status, String message) {
        return ApiError.builder()
                .timestamp(LocalDateTime.now())
                .status(status)
                .message(message)
                .build();
    }

    public static class DuffelServiceException extends Exception{
        public DuffelServiceException(String message) {
            super(message);
        }
    }

    public static class InvalidDateException extends Exception{
        public InvalidDateException(String message) {
            super(message);
        }
    }
}
