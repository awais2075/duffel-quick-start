package com.duffel.quick.start.dto.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DuffelApiError implements Serializable {
    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("errors")
    private List<Error> errors = null;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Error implements Serializable {
        @JsonProperty("type")
        private String type;
        @JsonProperty("title")
        private String title;
        @JsonProperty("source")
        private Source source;
        @JsonProperty("message")
        private String message;
        @JsonProperty("documentation_url")
        private String documentationUrl;
        @JsonProperty("code")
        private String code;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Meta implements Serializable {
        @JsonProperty("status")
        private Integer status;
        @JsonProperty("request_id")
        private String requestId;
    }

    @Data
    public static class Source {
        @JsonProperty("pointer")
        private String pointer;
        @JsonProperty("field")
        private String field;
    }
}
