package com.duffel.quick.start.dto;

public class Data<Model> {

    private Model data;

    public Model getData() {
        return data;
    }

    public void setData(Model data) {
        this.data = data;
    }
}
