package com.duffel.quick.start.dto.request;

import com.duffel.quick.start.enums.FieldType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Passenger {

    @JsonProperty("age")
    @Min(value = 0)
    @Max(value = 130)
    private Integer age;
    @JsonProperty("type")
    private FieldType type;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }
}