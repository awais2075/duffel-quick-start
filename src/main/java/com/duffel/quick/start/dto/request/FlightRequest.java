package com.duffel.quick.start.dto.request;

import com.duffel.quick.start.enums.CabinClass;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import java.util.List;


public class FlightRequest {

    @JsonProperty("slices")
    private List<Route> slices = null;
    @JsonProperty("passengers")
    @Valid
    private List<Passenger> passengers = null;
    @JsonProperty("cabin_class")
    private CabinClass cabinClass;

    public List<Route> getSlices() {
        return slices;
    }

    public void setSlices(List<Route> slices) {
        this.slices = slices;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public CabinClass getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(CabinClass cabinClass) {
        this.cabinClass = cabinClass;
    }
}
