package com.duffel.quick.start.service;

import com.duffel.quick.start.client.DuffelClient;
import com.duffel.quick.start.config.AppConfig;
import com.duffel.quick.start.dto.Data;
import com.duffel.quick.start.dto.request.FlightRequest;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DuffelService {

    @Autowired
    private DuffelClient duffelClient;
    @Autowired
    private AppConfig appConfig;

    public JsonNode searchFlights(FlightRequest flightRequest) {
        var data = new Data<FlightRequest>();
        data.setData(flightRequest);
        return duffelClient.searchFlights(appConfig.getHeaders(), data, false);
    }
}
